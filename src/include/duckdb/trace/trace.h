
#pragma once

 void _trace_constructor( void )
    __attribute__ ((no_instrument_function, constructor));

 void _trace_destructor( void )
    __attribute__ ((no_instrument_function, destructor));


extern "C" void __cyg_profile_func_enter(void *callee, void *caller) __attribute__ ((no_instrument_function));
extern "C" void __cyg_profile_func_exit(void *callee, void *caller) __attribute__ ((no_instrument_function));
