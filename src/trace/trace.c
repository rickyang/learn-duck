#include "duckdb/trace/trace.h"
#include <stdio.h>
#include <stdlib.h>


static FILE *fp = NULL;

void _trace_constructor(void)
{
  fp = fopen( "trace.txt", "w" );
  if (fp == NULL) exit(-1);
}

void _trace_deconstructor( void )
{
  if (fp) fclose( fp );
}

void __cyg_profile_func_enter( void *callee, void *call_site )
{
    if (!fp || !callee || !call_site) return;
    fprintf(fp, "enter %p from %p\n", callee, call_site);
}

void __cyg_profile_func_exit(void *callee, void *call_site)
{
    if (!fp || !callee || !call_site) return;
    fprintf(fp, "leave %p to %p\n", callee, call_site);
}
