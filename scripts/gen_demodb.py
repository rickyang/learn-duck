import duckdb
import random


def init_demodb(tbl):
    con = duckdb.connect(database='demo.duckdb', read_only=False)
    for i in range(1600):
        val = round(random.uniform(11.11, 177.77), 2)
        con.execute("INSERT INTO {} VALUES (?, ?, ?)".format(tbl), ['laptop_{}'.format(i), val, i])

def gen_demodb():
    con = duckdb.connect(database='demo.duckdb', read_only=False)
    con.execute("CREATE TABLE t1(item VARCHAR, value DECIMAL(10,2), count INTEGER)")
    con.execute("CREATE TABLE t2(item VARCHAR, value DECIMAL(10,2), count INTEGER)")
    con.execute("CREATE TABLE t3(item VARCHAR, value DECIMAL(10,2), count INTEGER)")

def query():
    con = duckdb.connect(database='demo.duckdb', read_only=False)
    con.execute("SELECT t1.item, t2.value, t2.count FROM t1 JOIN t2 ON t1.value=t2.value")
    print(con.fetchall())


if __name__ == '__main__':
    if True:
        gen_demodb()
        init_demodb('t1')
        init_demodb('t2')
        init_demodb('t3')
    
    query()

