import sys, os


elfFile = sys.argv[1]
cmd = 'nm -gD %s > %s.g' % (elfFile, elfFile)
os.system(cmd)

tsvLines = []
with open('%s.g' % elfFile, 'r') as f:
    for line in f:
        buf = line.strip().split(' ')
        if len(buf) < 3: continue
        tsvLines.append('\t'.join(buf) + '\n')

with open('%s.f' % elfFile, 'w') as f:
    f.writelines(tsvLines)

cmd = 'cat %s.f | c++filt -p > %s.sym' % (elfFile, elfFile)
os.system(cmd)

