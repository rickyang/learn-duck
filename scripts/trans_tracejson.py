import os, sys, json

ignore_abis = {
  "transaction clone for std": 69,
  "guard variable for std": 40,
  "__cxxabiv1": 51,
  "duckdb_jemalloc": 707,
  "duckdb_fmt": 23,
  "__gnu_cxx": 9381,
  "std": 89303,
  "operator new[]": 2,
  "operator new": 3,
  "construction vtable for std": 24,
  "typeinfo for std": 290,
  "typeinfo for duckdb_fmt": 6,
  "typeinfo for duckdb_httplib": 48,
  "typeinfo for duckdb_moodycamel": 7,
  "typeinfo for duckdb": 926,
  "typeinfo name for std": 294,
  "typeinfo name for duckdb_fmt": 6,
  "typeinfo name for duckdb_httplib": 48,
  "typeinfo name for duckdb_moodycamel": 7,
  "typeinfo name for duckdb": 926,
  "virtual thunk to std": 36,
  "vtable for duckdb_fmt": 4,
  "vtable for duckdb_httplib": 8,
  "vtable for duckdb_moodycamel": 6,
  "vtable for duckdb": 918,
  "vtable for __gnu_cxx": 7,
  "vtable for std": 267
}

def log2line(logFile, symFile):
  symbols = json.load(open(symFile, 'r'))
  outFile = open(logFile + '.trace', 'w')
  with open(logFile, 'r') as fobj:
      cnt = 0
      for line in fobj:
          cnt += 1
          buf = line.strip().split('\t')
          action, func_a, func_b = buf[0], buf[1], buf[3]
          nline = line
          if func_a in symbols:
            nline = nline.replace(func_a, symbols[func_a])
          if func_b in symbols:
            nline = nline.replace(func_b, symbols[func_b])
          outFile.write(nline)

def log2trace(logFile, symFile):
  events = []
  symbols = json.load(open(symFile, 'r'))
  functions = {}
  skip_funcs = {'duckdb::DuckDB::DuckDB': 1}
  ignore_scope = []
  with open(logFile, 'r') as fobj:
      cnt, cost, ts = 0, 1000, 0
      for line in fobj:
          cnt += 1
          buf = line.strip().split(' ')
          action, func_a, func_b = buf[0], buf[1], buf[3]
          if func_a in symbols:
            func_a = symbols[func_a]
            if func_a in ignore_abis: continue
          if func_b in symbols:
            func_b = symbols[func_b]
            print(func_b)
          if func_a not in functions:
            functions[func_a] = 0
          functions[func_a] += 1

          if action == 'enter':
            if func_a in skip_funcs:
              ignore_scope.append(func_a)
            elif len(ignore_scope) == 0:
              events.append({"name": func_a, "ph": "B", "pid": 'Main', "tid": func_a, "ts": ts+100})
          elif action == 'leave':
            if func_a in skip_funcs and len(ignore_scope) > 0 and ignore_scope[-1] == func_a:
              ignore_scope.pop()
            elif len(ignore_scope) == 0:
              ets = ts+cost
              events.append({"name": func_a, "ph": "E", "pid": 'Main', "tid": func_a, "ts": ets})
              ts += cost
  with open(logFile+'.trace.json', 'w') as fobj:
    fobj.write(json.dumps({'traceEvents': events}, indent=2))
  with open(logFile+'.functions.json', 'w') as fobj:
    fobj.write(json.dumps(functions, indent=2))



if __name__ == '__main__':
  logFile = sys.argv[1]
  symFile = sys.argv[2]
  # log2line(logFile, symFile)
  log2trace(logFile, symFile)


