import os, sys, json

ignore_abis = {
  "transaction clone for std": 69,
  "guard variable for std": 40,
  "__cxxabiv1": 51,
  "duckdb_jemalloc": 707,
  "duckdb_fmt": 23,
  "__gnu_cxx": 9381,
  "std": 89303,
  "construction vtable for std": 24,
  "typeinfo for std": 290,
  "typeinfo for duckdb_fmt": 6,
  "typeinfo for duckdb_httplib": 48,
  "typeinfo for duckdb_moodycamel": 7,
  "typeinfo for duckdb": 926,
  "typeinfo name for std": 294,
  "typeinfo name for duckdb_fmt": 6,
  "typeinfo name for duckdb_httplib": 48,
  "typeinfo name for duckdb_moodycamel": 7,
  "typeinfo name for duckdb": 926,
  "virtual thunk to std": 36,
  "vtable for duckdb_fmt": 4,
  "vtable for duckdb_httplib": 8,
  "vtable for duckdb_moodycamel": 6,
  "vtable for duckdb": 918,
  "vtable for __gnu_cxx": 7,
  "vtable for std": 267
}

abis = {}
addrs = {}
fname = sys.argv[1]
with open(fname, 'r') as fobj:
    for line in fobj:
        buf = line.strip().split('\t')
        addr = '0x' + buf[0].lstrip('0')
        abi = buf[2].split('::')[0]
        if abi not in abis: abis[abi] = 0
        abis[abi] += 1
        func = buf[2] if abi not in ignore_abis else abi
        addrs[addr] = func

print(json.dumps(abis, indent=2))
with open(fname+'.addr', 'w') as fobj:
    fobj.write(json.dumps(addrs))
