extern crate iron;
extern crate staticfile;

use staticfile::Static;
use iron::Iron;
use mount::Mount;
use std::path::Path;

fn main() {
    println!("trace UI");

    let mut mount = Mount::new();
    mount.mount("/", Static::new(Path::new("web/")));
    println!("visit URL: http://127.0.0.1:6688 in Chrome !");
    Iron::new(mount).http("127.0.0.1:6688").unwrap();
}
